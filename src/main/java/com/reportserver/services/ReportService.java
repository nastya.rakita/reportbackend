package com.reportserver.services;

import com.reportserver.models.ReportData;
import com.reportserver.repositories.ReportRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReportService {

    private final ReportRepository repository;

    public ResponseEntity<?> addReport(ReportData reportData) {
        try{
           return this.repository.save(reportData).getName().equals(reportData.getName())?
                   new ResponseEntity<>(true,HttpStatus.OK):
                   new ResponseEntity<>(false,HttpStatus.BAD_REQUEST);
        }
        catch (DataAccessException ex) {
            log.error("Report was not added. Database error", ex);
            throw ex;
        }
    }

    public ResponseEntity<?> updateReport(ReportData reportData) {
        try {
            if (this.repository.findById(reportData.getId()).isPresent()) {
                return this.repository.save(reportData).getId().equals(reportData.getId()) ?
                        new ResponseEntity<>(true, HttpStatus.OK) :
                        new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
            }
        } catch (DataAccessException ex) {
            log.error("Report was not added. Database error", ex);
            throw ex;
        }
    }

    public ResponseEntity<List<ReportData>> getAllReports() {
        try {
            List<ReportData> reportDataList = new ArrayList<>();
            repository.findAll().forEach(reportDataList::add);
            return new ResponseEntity<>(reportDataList, HttpStatus.OK);
        } catch (DataAccessException ex) {
            log.error("Error while searching for all reports. Database error", ex);
            throw ex;
        }
    }
}
