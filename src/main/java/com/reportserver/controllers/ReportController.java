package com.reportserver.controllers;

import com.reportserver.models.ReportData;
import com.reportserver.services.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "report")
public class ReportController {
    private final ReportService reportService;

    @PostMapping
    public ResponseEntity<?> addReport(@RequestBody ReportData reportData) {
        return this.reportService.addReport(reportData);
    }

    @PutMapping
    public ResponseEntity<?> updateReport(@RequestBody ReportData reportData) {
        return this.reportService.updateReport(reportData);
    }

    @GetMapping
    public ResponseEntity<List<ReportData>> getAllReports() {
        return this.reportService.getAllReports();
    }
}
