package com.reportserver.repositories;

import com.reportserver.models.ReportData;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ReportRepository extends CrudRepository<ReportData, Integer> {

    Optional<ReportData> findById(Integer id);
}


