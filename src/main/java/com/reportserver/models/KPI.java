package com.reportserver.models;

enum KPI {
    TWAMP_Availability,
    TWAMP_Jitter,
    TWAMP_Jitter_Far,
    TWAMP_Jitter_Near,
    TWAMP_Latency,
    TWAMP_Latency_Far,
    TWAMP_Latency_Near,
    TWAMP_Packets,
    TWAMP_Tests,
    TWAMP_Thresholds
}
