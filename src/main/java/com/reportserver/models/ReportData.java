package com.reportserver.models;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "reports")
public class ReportData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_private")
    private boolean isPrivate;

    @Column(name = "description")
    private String description;

    @ElementCollection(targetClass = KPI.class)
    @CollectionTable(name = "report_kpi",
            joinColumns = @JoinColumn(name = "report_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "kpi_name")
    private Set<KPI> kpiList;
}
