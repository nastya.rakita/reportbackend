package com.reportserver.models;

import java.util.List;

public class ReportRequest {
    private String name;
    private boolean isPrivate;
    private String description;
    private List<KPI> kpiList;
}
