-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.0-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db
CREATE DATABASE IF NOT EXISTS `db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db`;

-- Dumping structure for table db.kpi
CREATE TABLE IF NOT EXISTS `kpi` (
  `kpi_id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi_name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kpi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table db.kpi: ~10 rows (approximately)
/*!40000 ALTER TABLE `kpi` DISABLE KEYS */;
INSERT INTO `kpi` (`kpi_id`, `kpi_name`) VALUES
	(1, 'TWAMP_Availability'),
	(2, 'TWAMP_Jitter'),
	(3, 'TWAMP_Jitter_Far'),
	(4, 'TWAMP_Jitter_Near'),
	(5, 'TWAMP_Latency'),
	(6, 'TWAMP_Latency_Far'),
	(7, 'TWAMP_Latency_Near'),
	(8, 'TWAMP_Packets'),
	(9, 'TWAMP_Tests'),
	(10, 'TWAMP_Thresholds');
/*!40000 ALTER TABLE `kpi` ENABLE KEYS */;

-- Dumping structure for table db.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `is_private` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table db.reports: ~5 rows (approximately)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`id`, `name`, `description`, `is_private`) VALUES
	(1, 'User7', 'some text2', 0),
	(2, 'USER2', 'description2', 1),
	(3, 'USER3', 'description3', 1),
	(4, 'USER4', 'description4', 1),
	(5, 'User7', 'some text', 0),
	(6, 'User7', 'some text2', 0);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping structure for table db.report_kpi
CREATE TABLE IF NOT EXISTS `report_kpi` (
  `report_id` int(11) NOT NULL,
  `kpi_name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db.report_kpi: ~5 rows (approximately)
/*!40000 ALTER TABLE `report_kpi` DISABLE KEYS */;
INSERT INTO `report_kpi` (`report_id`, `kpi_name`) VALUES
	(1, 'TWAMP_Availability'),
	(1, 'TWAMP_Jitter_Far'),
	(1, 'TWAMP_Latency'),
	(2, 'TWAMP_Latency_Near'),
	(3, 'TWAMP_Tests'),
	(6, 'TWAMP_Latency_Far');
/*!40000 ALTER TABLE `report_kpi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
